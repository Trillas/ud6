package Activitat7;

import javax.swing.JOptionPane;

public class Activitat7App {

	public static void main(String[] args) {
		//Pregunto cuanto dinero quiere convertir i a que moneda
		String divisa;
		String textoDinero = JOptionPane.showInputDialog(null, "Pon cuanto dinero tienes");
		int dinero = Integer.parseInt(textoDinero);
		
		do {
			divisa = JOptionPane.showInputDialog(null, "A que moneda lo quieres convertir (libras, dolares, yenes)");
		} while (!divisa.equalsIgnoreCase("libras") && !divisa.equalsIgnoreCase("dolares") && !divisa.equalsIgnoreCase("yenes"));
		
		conversion(dinero, divisa);
	}
	public static void conversion (double dinero, String convertir) {
		switch (convertir) {
		case "libras":
			JOptionPane.showMessageDialog(null, dinero + "� en " + convertir + " es: " + (dinero*0.86));
			break;
		case "dolares":
			JOptionPane.showMessageDialog(null, dinero + "� en " + convertir + " es: " + (dinero*1.28611));
			break;
		case "yenes":
			JOptionPane.showMessageDialog(null, dinero + "� en " + convertir + " es: " + (dinero*129.852));
			break;
		}
	}

}
