package Activitat11;

import javax.swing.JOptionPane;

public class Activitat11App {

	public static void main(String[] args) {
		//Pregunto el tama�o del array
        String texto=JOptionPane.showInputDialog("Introduce un tama�o");
        int tamanio=Integer.parseInt(texto);
 
        //Creamos los arrays
        int array1[]=new int [tamanio];
        int array2[];
 
        //Rellenamos el array1
        rellenarNumAleatorioArray(array1, 10, 100);
 
        //Copio el array 2 en el 1 y borro el 1
        array2=array1;
        array1=new int[tamanio];
 
        //Lo relleno otra vez aleatorio
        rellenarNumAleatorioArray(array1, 10, 100);
 
        //Hago las operaciones i las guardo en el array 3
        int array3[]=multiplicador(array1, array2);
 
        //Ense�o los arrays
 
        System.out.println("Array1");
        mostrarArray(array1);
        System.out.println(" ");
 
        System.out.println("Array2");
        mostrarArray(array2);
        System.out.println(" ");
 
        System.out.println("Array3");
        mostrarArray(array3);
        System.out.println(" ");
 
		
	}
	public static void rellenarNumAleatorioArray(int lista[], int a, int b){
        for(int i=0;i<lista.length;i++){
            //Generamos los numeros aleatorios
            lista[i]=((int)Math.floor(Math.random()*(a-b)+b));
        }
    }
 
	public static void mostrarArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("La posicion " + (i+1) + " es: " + array[i]);
		}
	}
 
    public static int[] multiplicador(int array1[], int array2[]){
        int array3[]=new int[array1.length];
        for(int i=0;i<array1.length;i++){
            array3[i]=array1[i]*array2[i];
        }
        return array3;
    }
}
