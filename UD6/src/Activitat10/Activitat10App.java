package Activitat10;

import javax.swing.JOptionPane;

public class Activitat10App {

	public static void main(String[] args) {
		//Pregunto el tama�� de l'array
		String textoTama�oArray = JOptionPane.showInputDialog(null, "De que tama�o quieres el array");
		int tama�oArray = Integer.parseInt(textoTama�oArray);
		String textoNumMin = JOptionPane.showInputDialog(null, "Pon el n�mero m�nimo de el aleatorio");
		int numMin = Integer.parseInt(textoNumMin);
		String textoNumMax= JOptionPane.showInputDialog(null, "Pon el n�mero m�xima de el aleatorio");
		int numMax = Integer.parseInt(textoNumMax);
		//Creo el array
		int[] array = new int [tama�oArray];
		array = rellenarArray(numMin,numMax,array);
		mostrarArray(array);
		System.out.println("El n�mero m�s grande del array es: " + buscarElMayor(array));

	}
	//Muestro el array
	public static void mostrarArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("La posicio " + (i+1) + " es: " + array[i]);
		}
	}
	//Lo lleno de n�meros aleatorios llamando a la otra funci�n
	public static int[] rellenarArray(int min, int max, int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = numeroAleatorio(min,max);
		}
		return array;
	}
	//Genera n�meros aleatorios
	private static int numeroAleatorio (int min, int max) {
		max++;
		int num ;
		boolean esPrimo;
		do {
			num = (int)(Math.random() * max) + min;
			esPrimo = numeroPrimo(num);
		} while (!esPrimo);
		return num;
	}
	//Miro que sea un n�mero primo
	public static boolean numeroPrimo (int num) {
		int contador = 2;
		boolean primo = true;
		while ((primo) && (contador!=num)){
			if (num % contador == 0)
		      primo = false;
		      contador++;
		}
	    return primo;
	}
	//Busco el mayor en toda la array
	public static int buscarElMayor (int[] array) {
		int aux = 0;
		for (int i = 0; i < array.length; i++) {
			if (aux < array[i]) {
				aux = array[i];
			}
		}
		return aux;
	}


}
