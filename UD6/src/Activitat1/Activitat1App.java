package Activitat1;

import javax.swing.JOptionPane;

public class Activitat1App {

	public static void main(String[] args) {
		//Declaro las variables que voy a utilitzar
		String queAreaCalcular;
		boolean salir = true;
		double resultado;
		//Hago un bucle para assegurarme que ponga un dato v�lido
		do {
			queAreaCalcular = JOptionPane.showInputDialog(null, "Que �rea quieres calcular?(circulo, triangulo, cuadrado)");
			if (queAreaCalcular.equalsIgnoreCase("circulo") || queAreaCalcular.equalsIgnoreCase("triangulo") || queAreaCalcular.equalsIgnoreCase("cuadrado")) {
				salir = false;
			}else {
				JOptionPane.showMessageDialog(null, "No has introducido un valor valido, prueba otra vez");
			}
		} while (salir);
		//Seg�n lo que ha puesto lanzare un m�todo o otro
		if (queAreaCalcular.equalsIgnoreCase("circulo")) {
			resultado = areaCirculo();
		}else if(queAreaCalcular.equalsIgnoreCase("triangulo")) {
			resultado = areaTriangulo();
		}else {
			resultado = areaCuadrado();
		}
		//Muestro por pantalla el resultado
		JOptionPane.showMessageDialog(null, "La area de tu " + queAreaCalcular + " es de: " + resultado);

	}
	//Aqui hago una funci�n para cada forma, pido las cosas dentro de la funci�n i devuelvo la �rea
	public static double areaCirculo() {
		String textoRadio = JOptionPane.showInputDialog(null, "Pon el radio del c�rculo");
		double radio = Double.parseDouble(textoRadio);
		return (Math.pow(radio, 2) * Math.PI);
	}
	public static double areaTriangulo() {
		String textoBase = JOptionPane.showInputDialog(null, "Pon la base del triangulo");
		double base = Double.parseDouble(textoBase);
		String textoAltura = JOptionPane.showInputDialog(null, "Pon la altura del triangulo");
		double altura = Double.parseDouble(textoAltura);
		return ((base * altura) / 2);
	}
	public static double areaCuadrado() {
		String textoLado = JOptionPane.showInputDialog(null, "Pon el lado del cuadrado");
		double lado = Double.parseDouble(textoLado);
		return (lado * lado);
	}
}
