package Activitat6;

import javax.swing.JOptionPane;

public class Activitat6App {

	public static void main(String[] args) {
		//Pido el n�mero i compruevo que es positivo
		int numero;
		do {
			String textoNumero = JOptionPane.showInputDialog(null, "Pon un n�mero");
			numero = Integer.parseInt(textoNumero);
		} while (numero <= 0);
		
		//Muestro por pantalla el resultado
		JOptionPane.showMessageDialog(null, "el " + numero  + " tiene: " + numeroCifras(numero) + " cifras.");
	}
	public static int numeroCifras (int numero) {
		String texto = Integer.toString(numero);
		return texto.length();
	}

}
