package activitat9;

import javax.swing.JOptionPane;

public class Activitat9App {

	public static void main(String[] args) {
		//Pregunto el tama�� de l'array
		String textoTama�oArray = JOptionPane.showInputDialog(null, "De que tama�o quieres el array");
		int tama�oArray = Integer.parseInt(textoTama�oArray);
		//Creo el array
		int[] array = new int [tama�oArray];
		array = rellenarArray(0,10,array);
		mostrarArray(array);

	}
	//Muestro el array con el total de todos los n�meros
	public static void mostrarArray(int[] array) {
		int total = 0;
		for (int i = 0; i < array.length; i++) {
			System.out.println("La posicio " + (i+1) + " es: " + array[i]);
			total = total + array[i];	
		}
		System.out.println("El total del array es: " + total);
	}
	//Para rellenar el array de n�meros aleatorios
	public static int[] rellenarArray(int min, int max, int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = numeroAleatorio(min,max);
		}
		return array;
	}
	//Genero un n�mero aleatorio
	private static int numeroAleatorio (int min, int max) {
		return (int)(Math.random() * max) + min;
	}
}
