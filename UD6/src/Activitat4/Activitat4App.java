package Activitat4;

import javax.swing.JOptionPane;

public class Activitat4App {

	public static void main(String[] args) {
		// Pido el n�mero
		String textoNumero = JOptionPane.showInputDialog(null, "Pon un n�mero");
		int numero = Integer.parseInt(textoNumero);
		
		JOptionPane.showMessageDialog(null, "El factorial de " + numero + " es: " + factorial(numero));

	}
	public static int factorial(int num) {
		int total = 1;
		for (int i = num; i > 0; i--) {
			total = total * i;
		}
		return total;
	}

}
