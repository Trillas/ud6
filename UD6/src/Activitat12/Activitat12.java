package Activitat12;

import javax.swing.JOptionPane;

public class Activitat12 {

	public static void main(String[] args) {
		//Preguntamos el tamaño del array
		int numero;
		String textoTamañoArray = JOptionPane.showInputDialog(null, "De que tamaño quieres el array");
		int tamañoArray = Integer.parseInt(textoTamañoArray);
	
		do {
			String textoNumero = JOptionPane.showInputDialog(null, "Pon un numero para comprovar");
			numero = Integer.parseInt(textoNumero);
		} while (numero < 0 || numero > 9);
		
		//Creo el array
		int[] array1 = new int [tamañoArray];
		
		//Relleno el array
		array1 = rellenarArray(1, 300, array1);
		
		//relleno el segundo array con las comprovaciones
		int[] array2 = new int [tamañoSegundoArray(array1,numero)];
		array2 = rellenarArray2(array1, array2, numero);
		
		//Muestro los arrays
		System.out.println("Array1");
		System.out.println(" ");
		mostrarArray(array1);
		System.out.println(" ");
		System.out.println(" ");
		System.out.println("Array2");
		mostrarArray(array2);

	}
	public static int[] rellenarArray2 (int[] array,int[] array2, int numeroBuscado) {
		int contadorArray = 0;
		for (int i = 0; i < array.length; i++) {
			if (numeroBuscado == array[i]-(array[i]/10*10)) {
				array2[contadorArray] = array[i];
				contadorArray++;
			}
		}
		return array2;
	}
	public static int tamañoSegundoArray (int[] array, int numeroBuscado) {
		int contador = 0;
		for (int i = 0; i < array.length; i++) {
			if (numeroBuscado == array[i]-(array[i]/10*10)) {
				contador++;
			}
		}
		return contador;
	}
	//Para rellenar el array de números aleatorios
	public static int[] rellenarArray(int min, int max, int[] array) {
		for (int i = 0; i < array.length; i++) {
			array[i] = numeroAleatorio(min,max);
		}
	return array;
	}
	//Genero un número aleatorio
	private static int numeroAleatorio (int min, int max) {
		max++;
		return (int)(Math.random() * max) + min;
	}
	//para mostrar los arrays
	public static void mostrarArray(int[] array) {
		for (int i = 0; i < array.length; i++) {
			System.out.println("La posicion " + (i+1) + " es: " + array[i]);
		}
	}

}
