package Activitat8;

import javax.swing.JOptionPane;

public class Activitat8App {

	public static void main(String[] args) {
		//LLamo a las dos funciones
		mostrarValores(rellenarValores());
		
	}
	public static int[] rellenarValores () {
		//Creo el array y le pido los valores por teclado
		int[] array = new int [10];
		String stringAux;
		int intAux;
		for (int i = 0; i < array.length; i++) {
			stringAux = JOptionPane.showInputDialog(null, "Pon el valor de la posici�n " + (i + 1));
			intAux = Integer.parseInt(stringAux);
			array[i] = intAux;
		}
		return array;
	}
	public static void mostrarValores (int[] array) {
		//Muestro los valores
		for (int i = 0; i < array.length; i++) {
			System.out.println("La posici�n " + (i+1) + " es: " + array[i]);
		}
	}

}
