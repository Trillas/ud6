package Activitat3;

import javax.swing.JOptionPane;

public class Activitat3App {

	public static void main(String[] args) {
		//Pido un n�mero
		String textoNumero = JOptionPane.showInputDialog(null, "Pon un n�mero");
		int numero = Integer.parseInt(textoNumero);
		//Muestro el resultado
		JOptionPane.showMessageDialog(null, "El numero es primo? " + numeroPrimo(numero));
	}
	public static boolean numeroPrimo (int num) {
		//Aqui lo que hago �s dividirlo con todos los numeros hasta el numero que se ha dicho, por cada vez
		//que se divide y da 0 lo pongo en un contador, si el contador supera 2 ya no ser� primo
		//ya que si fuera primo solo se puede dividir hasta 2 veces con el 1 y el n�mero mismo
		int contador = 2;
		boolean primo = true;
		while ((primo) && (contador!=num)){
			if (num % contador == 0)
		      primo = false;
		      contador++;
		}
	    return primo;
	}

}
