package Activitat5;

import javax.swing.JOptionPane;

public class Activitat5App {

	public static void main(String[] args) {
		// Pido el n�mero
		String textoNumero = JOptionPane.showInputDialog(null, "Pon un n�mero");
		int numero = Integer.parseInt(textoNumero);
		//Muestro por pantalla el resultado
		JOptionPane.showMessageDialog(null, "el " + numero  + " en binario es: " + pasarBinario(numero));

	}
	public static String pasarBinario (int numero) {
		//Aqui he encontrado el StringBuilder que sirve para poder ir a�adiendo valores en un String
		//con este m�todo he ido a�adiendo los valors que me dava en el while dentro del String
		StringBuilder binario = new StringBuilder();
		int residuo;
        while (numero > 0) {
            residuo = (int) (numero % 2);
            numero = numero/ 2;

            binario.insert(0, String.valueOf(residuo));
        }
        return binario.toString() ;
	}

}
