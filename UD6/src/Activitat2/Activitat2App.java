package Activitat2;

import javax.swing.JOptionPane;

public class Activitat2App {

	public static void main(String[] args) {
		//Pido todos los datos que necesitare
		String textoCuantosNumeros = JOptionPane.showInputDialog(null, "Quantos quieres generar");
		int cuantosNumeros = Integer.parseInt(textoCuantosNumeros);
		String textoNumeroMin = JOptionPane.showInputDialog(null, "Cual es el n�mero m�nimo");
		int numeroMin = Integer.parseInt(textoNumeroMin);
		String textoNumeroMax = JOptionPane.showInputDialog(null, "Cual es el n�mero m�ximo");
		int numeroMax = Integer.parseInt(textoNumeroMax);
		//Hago un bucle para mostrar los n�meros generados
		for (int i = 0; i < cuantosNumeros; i++) {
			System.out.println(numeroAleatorio(numeroMin, numeroMax));
		}
	}
	public static int numeroAleatorio(int min, int max) {
		//Genero un n�mero aleatorio
		return (int)(Math.random() * max) + min;
	}
	

}
